﻿namespace FlightManager.Lib.Abstract
{
	internal interface IDataManager<T>
	{
		void Create(T obj);

		void Delete(T obj);

		void DeleteAll();
	}
}
